# MiniX5

In MiniX5, I have created a grid where the cells are filled with random colors every 200 milliseconds.

The two simple rules you wanted to implement in the code are:
1. A random cell should be filled. 
2. The color of the cell should also be random.


In the program i have made, rules and processes set the starting point and how things change over time. 

The main rule is simple: randomly color a cell. But when this rule interacts with the grid structure and random cell selection, it leads to complex and unpredictable patterns.

Here's how it works:
1. Setting Up the Grid: At the beginning, the grid is filled with white cells arranged in a grid pattern. This is done by the generateArt() function.
2. Coloring Cells: As time passes, the program picks a random cell in the grid and changes its color to a random RGB value. This happens every 200 milliseconds, controlled by the setInterval(changeRandomCellColor, 200) function in the setup().
3. Emergence of Patterns: The result is a dynamic display of colors that constantly changes. Even though each cell's color is random, interesting patterns and structures may emerge over time as neighboring cells influence each other.



![Billede af min MiniX5](minix5.png "random colors")
![Billede af min MiniX5](minix-5.png "random colors")
<br>

Please run the code [[here](https://tuva-ap.gitlab.io/aesthetic-programming/miniX5)]
<!--- Linket skal formatteres sådan som jeg har gjort det - https://{GRUPPE NAVN}.gitlab.io/{PROJEKT NAVN}{miniX DER SKAL SES}-->

https://tuva-ap.gitlab.io/aesthetic-programming/miniX5
<br>
Please view the full repository [https://gitlab.com/tuva-ap/aesthetic-programming]
<!--- Det her link kan I bare kopiere fra jeres browser, kopier linked når I befinder jer I mappen -->

### **References**
<!-- Eksempler på referencer her, pretty much bare links med line breaks mellem dem-->
<br>
[Random](https://p5js.org/reference/#/p5/random)
<br>
Soon Winnie & Cox Geoff (2020), "Data Capture", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press(Chapter 5)





