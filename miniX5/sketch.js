let gridSize = 50;
let rows, cols;

function setup() {
  createCanvas(800, 600);
  rows = height / gridSize;
  cols = width / gridSize;
  generateArt();
  setInterval(changeRandomCellColor, 200); // Endre en tilfeldig cellefarge hvert 200 milli sec
}

function generateArt() {
  // Tegn det hvite rutenettet
  for (let x = 0; x < width; x += gridSize) {
    for (let y = 0; y < height; y += gridSize) {
      fill(255); // Fyll med hvitt
      rect(x, y, gridSize, gridSize);
    }
  }
}

function changeRandomCellColor() {
  // Velg en tilfeldig celle
  let randomRow = floor(random(rows));
  let randomCol = floor(random(cols));
  let x = randomCol * gridSize;
  let y = randomRow * gridSize;

  // Tegn en tilfeldig farge i den valgte cellen
  for (let i = 0; i < rows; i++) {
    for (let j = 0; j < cols; j++) {
      let cellX = j * gridSize;
      let cellY = i * gridSize;


      // If-statement som sørger for at den tilfeldig valgte cellen får en tilfeldig farge, mens resten av cellene i rutenettet forblir hvite.
      if (cellX === x && cellY === y) {
        let r = floor(random(256)); // Tilfeldig rød verdi mellom 0 og 255
        let g = floor(random(256)); // Tilfeldig grønn verdi mellom 0 og 255
        let b = floor(random(256)); // Tilfeldig blå verdi mellom 0 og 255
        fill(r, g, b);
        rect(x, y, gridSize, gridSize);
      }
    }
  }
}




