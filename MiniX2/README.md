In my miniX project, I looked into emojis and how we use them every day to express ourselves. While searching online, I found a website that sorted emojis by skin color. There were categories like "pale emojis," "cream white emojis," "brown emojis," "dark brown emojis," and "black emojis." Strangely, yellow emojis were in a category called "people and fantasy."

This got me thinking about what emojis and their colors really mean. In my project, I tried to make emojis that already exist, mostly the yellow ones I usually use. I realized that even though we might not think much about it, emojis have some issues with how they represent different skin colors. Yellow is seen as a "neutral" color, but it doesn't cover all the different skin tones. This leaves out a lot of people because yellow is considered a light color and doesn't include the darker skin tones.

People have tried to make emojis more inclusive by adding different skin tones, but it's clear that we need to keep talking about this to make sure our digital tools and emojis include everyone and show all of us.

Please run the code [[here](https://tuva-ap.gitlab.io/aesthetic-programming/miniX2/index.html)]

references:

https://editor.p5js.org/YanlinMa/sketches/HJgjZIu2

https://getemoji.com/
