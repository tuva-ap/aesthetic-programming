# MiniX4

"SmileyCam: Capturing Joy Through Facial Expression"

SmileyCam is an interactive program that captures joy through facial expression. Using a webcam, users are encouraged to smile, triggering a cheerful smiley face on the screen. This project uses JavaScript and the p5.js library for webcam access and facial tracking, along with the clmtrackr library for real-time face detection. Through this project, users explore the meeting point of technology and human emotion, engaging in an interactive experience that encourages positivity and self-expression.

When a user smiles in front of the webcam, the program detects the smile and displays a smiley face on the screen. The user can observe their own smile reflected back at them, creating an interactive and engaging experience.

Through this project, I learned about real-time face detection and tracking using webcams. I gained insights into JavaScript programming, particularly in the context of creative coding and interactive experiences. Additionally, I explored the relationship between technology and human emotion, considering how software can be used to capture and reflect emotional states.

Data capture, particularly in the context of facial recognition and emotion detection, raises important cultural implications related to privacy, consent, and representation. While projects like SmileyCam offer fun and engaging experiences, they also highlight the need for ethical considerations in data collection and usage. It's essential to prioritize user privacy and ensure transparency in how data is collected, stored, and utilized to mitigate potential risks and promote trust in technology.


![Billede af min MiniX4](minix4-.png "smiley")
![Billede af min MiniX4](%20minix4.png "smiley")
<br>

Please run the code [[here](file:///Users/tuvaelnan/Downloads/Div%20programming/MiniX4/index.html)]
<!--- Linket skal formatteres sådan som jeg har gjort det - https://{GRUPPE NAVN}.gitlab.io/{PROJEKT NAVN}{miniX DER SKAL SES}-->
<br>
Please view the full repository [https://gitlab.com/tuva-ap/aesthetic-programming]
<!--- Det her link kan I bare kopiere fra jeres browser, kopier linked når I befinder jer I mappen -->

### **References**
<!-- Eksempler på referencer her, pretty much bare links med line breaks mellem dem-->
<br>
[Video capture](https://p5js.org/reference/#/p5/createCapture)
<br>
[Smiley](https://editor.p5js.org/mrm029/sketches/BJMVVsHJN)
<br>
German sin kode fra forelesning hehe





