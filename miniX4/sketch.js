
let videoCapture;

function createVideoCapture() {
  videoCapture = createCapture(VIDEO);
  videoCapture.size(640, 480);
  videoCapture.hide();
}

let microphone;

function createAudioCapture() {
  microphone = new p5.AudioIn();
  microphone.start();
}

let cameraTracker;

function setupFaceTracker() {
  cameraTracker = new clm.tracker();
  cameraTracker.init(pModel);
  cameraTracker.start(videoCapture.elt);
}

let smiling = false;

function setup() {
  createCanvas(640, 560); // Legg til litt ekstra høyde for teksten
  createVideoCapture();
  createAudioCapture();
  setupFaceTracker();
}

function draw() {
 let volumeLevel = microphone.getLevel();

  image(videoCapture, 0, 0, 640, 480);

  // tegne opp ansikt, "facetracker"

  let positions = cameraTracker.getCurrentPosition();
  if (positions.length) {
 
    for (let i = 0; i < positions.length; i++) {
      noStroke();
      fill(map(positions[i][0], 0, width, 100, 255), 0, 0, 120);
      // Tegn en sirkel på hvert punkt
      ellipse(positions[i][0], positions[i][1], 5, 5);
    }

    // Se om brukeren smiler i kamera
    let mouthOpen = positions[57][1] - positions[60][1];
    if (mouthOpen > 10) {
      smiling = true;
    } else {
      smiling = false;
    }
  }

  // Smiley om brukeren smiler
  if (smiling) {
    fill(255, 255, 0); // Gul
    ellipse(320, 240, 100, 100); // Hodet på smiley
    fill(0); // Svart
    ellipse(290, 220, 20, 20); // Venstre øye
    ellipse(350, 220, 20, 20); // Høyre øye
    noFill();
    stroke(0); // Svart
    strokeWeight(2);
    arc(320, 260, 60, 40, 0, PI); // Munn

    // Teksten under videoinput
    fill(0); // Farge for teksten
    textSize(24); // Størrelse på tekst
    textAlign(CENTER, TOP); // Plassering på tekst
    textLeading(36); // Mellomrom mellom bokstaver
    textFont('Verdana'); // Font stil
    textStyle(NORMAL); // Tykkelse
    text("smileeee!", width / 2, 500); // Plassering av tekst igjen
  }
}

