# Throbber MiniX3

Conceptually, the throbber design aims to convey a sense of anticipation and engagement while waiting for an action to complete. It serves as a visual cue to indicate that a process is ongoing. Additionally, the message "plant a flower while you wait" adds an element of interactivity inviting the user to participate in a simple activity while waiting. technically I have tried to use what we have learnt this week about loops, and the push and pop effects in P5.js. 

Throbbers, tell you that something is happening in the background. They keep you informed while you wait for a task to finish, like loading a webpage or a video. Throbbers help manage your expectations, so you know the system hasn't frozen. They often hide the technical stuff going on behind the scenes to keep things simple for you. Thats why it could be a good idea to remake the throbber to either match the deisgn of the webpage, or make something out of the waiting time, like I have in my MiniX assignment. 


![Billede af min MiniX1](MiniX3.png "Throbber")
![Billede af min MiniX1](MiniX3_.png "Throbber")
<br>

Please run the code [[here](https://tuva-ap.gitlab.io/aesthetic-programming/MiniX3/index.html)]
<!--- Linket skal formatteres sådan som jeg har gjort det - https://{GRUPPE NAVN}.gitlab.io/{PROJEKT NAVN}{miniX DER SKAL SES}-->
<br>
Please view the full repository [https://gitlab.com/tuva-ap/aesthetic-programming]
<!--- Det her link kan I bare kopiere fra jeres browser, kopier linked når I befinder jer I mappen -->

### **References**
<!-- Eksempler på referencer her, pretty much bare links med line breaks mellem dem-->
<br>
[Flowers pulses](https://p5js.org/examples/drawing-pulses.html)
[Throbber](file:///Users/tuvaelnan/Downloads/bookThrobbersketch%20(3).js)





