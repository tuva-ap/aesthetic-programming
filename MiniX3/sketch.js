let angle = 0;
let drawnShapes = []; // Array to store drawn shapes

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(102);
  noStroke();
  fill(0, 102);
  frameRate(8);
}

function draw() {
  background(252, 222, 244, 80);
  drawElements();
  textAlign(CENTER, CENTER);
  fill(255);
  textSize(20);
  textStyle(BOLD);
  text("While you wait, plant a flower on the screen<3", width / 2, height / 4);

  
  for (let i = 0; i < drawnShapes.length; i++) {
    let shape = drawnShapes[i];
    fill(shape.color);
    ellipse(shape.x, shape.y, shape.size, shape.size);
  }

  // Tegner når mouse is pressed
  if (mouseIsPressed === true) {
    angle += 5;
    let val = cos(radians(angle)) * 12.0;
    for (let a = 0; a < 360; a += 75) {
      let xoff = cos(radians(a)) * val;
      let yoff = sin(radians(a)) * val;
      fill(337, 121, 206); 
      ellipse(mouseX + xoff, mouseY + yoff, val, val);
      // Lagre firgurer som blir tegnet
      drawnShapes.push({ x: mouseX + xoff, y: mouseY + yoff, size: val, color: color(337, 121, 206) });
    }
    fill(255);
    ellipse(mouseX, mouseY, 2, 2);
    // Lagre figurer som blir tegnet
    drawnShapes.push({ x: mouseX, y: mouseY, size: 2, color: color(255) });
  }
}

function drawElements() {
  let num = 9;
  push();
  translate(width / 2, height / 2);
  // 360/num >> degree of each ellipse's movement;
  // frameCount%num >> get the remainder that to know which one
  // among 8 possible positions.
  let cir = (360 / num) * (frameCount % num);
  rotate(radians(cir));
  noStroke();
  fill(337, 121, 206);
  // the x parameter is the ellipse's distance from the center
  ellipse(35, 0, 22, 22);
  pop();
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}



