let pacmanSize = {
  w: 86,
  h: 89
};
let pacman;
let pacPosX; 
let mini_height;
let min_tofu = 5;  
let paperBalls = []; 
let score = 0, lose = 0;
let keyColor = 45;
let movingLeft = false;
let movingRight = false;
let moveSpeed = 10;

function preload(){
  pacman = loadImage("bin.png");
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  pacPosX = width / 2; // Start bin i midten av skjermen
  mini_height = height/2;
  for (let i = 0; i < min_tofu; i++) {
    paperBalls.push(new PaperBall());
  }
}

function draw() {
  background(255); 
  fill(keyColor, 255);
  rect(0, 0, width, 1); // Linjen på toppen av skjermen
  displayScore();
  showPaperBalls();
  image(pacman, pacPosX, height - pacmanSize.h, pacmanSize.w, pacmanSize.h);
  checkEating(); //scoring
  checkResult();
  
  // Sjekk om bin skal bevege seg
  if (movingLeft) {
    pacPosX -= moveSpeed;
  } else if (movingRight) {
    pacPosX += moveSpeed;
  }
  // Constrain pacman within the screen width
  pacPosX = constrain(pacPosX, 0, width - pacmanSize.w);
}

function showPaperBalls() { 
  for (let i = 0; i < paperBalls.length; i++) {
    paperBalls[i].move();
    paperBalls[i].show();
    if (paperBalls[i].pos.y > height) {
      lose++;
      paperBalls.splice(i, 1);
    }
  }
  // Add new paperballs if needed
  while (paperBalls.length < min_tofu) {
    paperBalls.push(new PaperBall());
  }
}

function checkEating() {
  for (let i = 0; i < paperBalls.length; i++) {
    let d = int(
      dist(pacPosX + pacmanSize.w/2, height - pacmanSize.h, 
        paperBalls[i].pos.x, paperBalls[i].pos.y)
      );
    if (paperBalls[i].pos.y > height - pacmanSize.h && // Papirballen skal treffe toppen av bøtta
        d < pacmanSize.w/2.5) {
      score++;
      paperBalls.splice(i,1);
    }
  }
}

function displayScore() {
    fill(255);
    stroke(keyColor);
    strokeWeight(2);
    textSize(17);
    text('You have recycled'  + score + " paperballs(s)", 10, 20);
    fill(keyColor,255);
    noStroke();
    text('PRESS the ARROW LEFT & RIGHT keys to move Bin',
    10, 40);
}

function checkResult() {
  if (lose > 2) { // Game over if more than 2 paperballs wasted
    fill(keyColor, 255);
    textSize(26);
    text("Too Little WASTAGE...GAME OVER!!!!", width/3, height/2);
    noLoop();
  }
}

function keyPressed() {
  if (keyCode === LEFT_ARROW) {
    movingLeft = true;
  } else if (keyCode === RIGHT_ARROW) {
    movingRight = true;
  }
}

function keyReleased() {
  if (keyCode === LEFT_ARROW) {
    movingLeft = false;
  } else if (keyCode === RIGHT_ARROW) {
    movingRight = false;
  }
}

// Define PaperBall class
class PaperBall {
  constructor() {
    this.pos = createVector(random(width), -50); // Start paperball at random x position above the screen
    this.vel = createVector(0, random(1, 3)); // Set random downward velocity
    this.size = 50; // Set paperball size
    this.img = loadImage("paperball.png"); // Load paperball image
  }

  move() {
    this.pos.add(this.vel); // Move paperball downward
  }

  show() {
    image(this.img, this.pos.x, this.pos.y, this.size, this.size); // Draw paperball
  }
}
