# MiniX6
In my game i have to main objects, the "pacman" that I have rennamed to "bin" and the Paperballs. 

1. Pacman (Bin): This object represents the main player character, which is depicted as a trash bin. It has attributes like position (pacPosX) and size (pacmanSize). Methods like movePacman() are responsible for moving pacman horizontally based on user input.

2. Paperballs (papirballer): These are the game objects that pacman needs to catch. They have attributes such as position (pos), size (size), and image (img). Methods like move() and show() are used to update their position and display them on the screen.

As the game runs, paperballs fall from the top of the screen while pacman moves horizontally. When a paperball hits the top of the trash bin (pacman), the score increases, and the paperball is removed from the screen. If the player misses the paperballs and allows them to reach the bottom of the screen, the count of missed paperballs increases. The goal is to earn as many points as possible by catching the paperballs before they reach the bottom.

These game objects interact with each other and the game environment to create the gameplay experience. The user controls pacman's horizontal movement, trying to catch as many paperballs as possible while avoiding misses. The game objects' behaviors and interactions are defined through the code, allowing for dynamic and engaging gameplay.

By gamifying the concept of waste reduction, this game not only provides entertainment but also educates and raises awareness about environmental importances. It encourages players to think critically about their consumption habits and to consider the consequences of waste generation. Ultimately, by promoting the message of "too little wastage," this game contributes to a broader cultural conversation about sustainability and responsible resource management.



![Billede af min MiniX6](minix6..png "TOO LITTLE WASTAGE!!")
![Billede af min MiniX6](minix6.png "layout")
<br>

Please run the code [[here](https://tuva-ap.gitlab.io/aestetic-programming/miniX6/index.html)]
<!--- Linket skal formatteres sådan som jeg har gjort det - https://{GRUPPE NAVN}.gitlab.io/{PROJEKT NAVN}{miniX DER SKAL SES}-->

https://tuva-ap.gitlab.io/aestetic-programming/miniX5/index.html
<br>
Please view the full repository [https://gitlab.com/tuva-ap/aesthetic-programming]
<!--- Det her link kan I bare kopiere fra jeres browser, kopier linked når I befinder jer I mappen -->

### **References**
<!-- Eksempler på referencer her, pretty much bare links med line breaks mellem dem-->
<br>
Soon Winnie & Cox Geoff (2020), "Data Capture", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press(Chapter 5)





