class Paperball {
  constructor() {
    this.speed = floor(random(3, 6));
    this.pos = createVector(width + 5, random(12, height / 1.7));
    this.size = floor(random(15, 35));
    this.ball_rotate = random(0, PI / 20);
    this.emoji_size = this.size / 1.8;
    this.img = loadImage("paperball.png");
  }

  move() {
    this.pos.x -= this.speed;
  }

  show() {
    push();
    translate(this.pos.x, this.pos.y);
    rotate(this.ball_rotate);
    image(this.img, -this.size / 2, -this.size / 2, this.size, this.size); 
    pop();
  }
}


    