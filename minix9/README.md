# MiniX9
Simplicity at the Communication Level
- SmileyCam provides users with a clear and intuitive interface, including real-time visual feedback such as a smiley face when a smile is detected.

Complexity at the Algorithmic Procedural Level
- The program uses advanced facial recognition and emotion detection algorithms, requiring efficient data handling and computational power to process real-time webcam data.Balancing real-time performance and accuracy can be difficult, as complex algorithms might slow down interactions. The program must prioritize user privacy and security while handling sensitive data.

Addressing Challenges
- The program should optimize its code for efficiency and implement robust error handling. Regularly gathering user feedback ensures an intuitive experience, while transparency about data collection and usage is essential.Individual flowcharts help clarify specific features, aiding in understanding and troubleshooting. Group flowcharts provide an overall view of the program's flow, highlighting interactions and user experience. Both are useful for balancing simplicity and complexity.


![Billede af min MiniX9](flowchart.png "flowchart")
<br>
Please view the full repository [https://gitlab.com/tuva-ap/aesthetic-programming]
<!--- Det her link kan I bare kopiere fra jeres browser, kopier linked når I befinder jer I mappen -->

### **References**
<!-- Eksempler på referencer her, pretty much bare links med line breaks mellem dem-->
<br>
Soon Winnie & Cox Geoff (2020), "Data Capture", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press(Chapter 5)





