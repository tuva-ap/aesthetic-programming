# MiniX1

Readme for MiniX1
For this mini exercise I have created a sketch with inspiration from what we typically know as optical illusion images. Using already created examples that I found in p5 references and modifyed them to fit my vision. The image showed in HTML is just a simple piece, that goes on by itself. I was also in the process thinking about making this piece ineractive, by adding more elements but quickly realised that my knowledge only goes to a certain point.
That is also something I have learned from my first coding experience. I still feel like I do not have all the knowledge I need to master the full library and references in P5. Although I will say that it has been more pleasant working with P5 than html, javascript and CSS. This because of the simplicity that lies in the reference list, and it feels like its easier to modify them, than creating new elements in ex. HTML.
Still feeling like this is all a bit new, and since it is a totally different and new language it will take time to fully get the grip of it. Hopefully it will come by time, with help from the lectures and the assigned readings each week.



![Billede af min MiniX1](minix1.png "Rotating stars")
<br>

Please run the code [[here](https://tuva-ap.gitlab.io/aesthetic-programming/miniX1/index.html)]
<!--- Linket skal formatteres sådan som jeg har gjort det - https://{GRUPPE NAVN}.gitlab.io/{PROJEKT NAVN}{miniX DER SKAL SES}-->

Please view the full repository [https://gitlab.com/tuva-ap/aesthetic-programming]
<!--- Det her link kan I bare kopiere fra jeres browser, kopier linked når I befinder jer I mappen -->

### **References**
<!-- Eksempler på referencer her, pretty much bare links med line breaks mellem dem-->
<br>
[Random](https://p5js.org/reference/#/p5/random)
<br>
Soon Winnie & Cox Geoff (2020), "Data Capture", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press(Chapter 5)

