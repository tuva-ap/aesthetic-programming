function setup() {
  createCanvas(500, 500);
}
function draw() {
  background(10, 10, 10);
  push();
  translate(width * 0.2, height * 0.5);
  rotate(frameCount / -10.0);
  star(0, 0, 30, 70, 17);
  pop();

  push(); 
  translate(width * 0.8, height * 0.5);
  rotate(frameCount / -5.0);
  star(0, 0, 30, 70, 20);
  pop();

  push();
  translate(width * 0.8, height * 0.8);
  rotate(frameCount / -10.0);
  star(0, 0, 30, 70, 15);
  pop();

  push();
  translate(width * 0.1, height * 0.1);
  rotate(frameCount / -10.0);
  star(0, 0, 30, 70, 5);
  pop();

  push();
  translate(width * 0.1, height * 0.8);
  rotate(frameCount / -10.0);
  star(0, 0, 30, 70, 20);
  pop();

  push();
  translate(width * 0.7, height * 0.2);
  rotate(frameCount / -10.0);
  star(0, 0, 30, 70, 10);

  pop();

  push();
  translate(width * 0.5, height * 0.9);
  rotate(frameCount / -10.0);
  star(0, 0, 30, 70, 6);
  fill (129, 199, 195);
  pop();

  push();
  translate(width * 0.5, height * 0.6);
  rotate(frameCount / -10.0);
  star(20, 0, 30, 70, 13);
  pop();

  push();
  translate(width * 0.3, height * 0.3);
  rotate(frameCount / -10.0);
  star(0, 0, 30, 70, 8);
  pop();

  push();
  translate(width * 0.45, height * 0.1);
  rotate(frameCount / -10.0);
  star(0, 0, 30, 70, 15);
  pop();

}
function star(x, y, radius1, radius2, npoints) {
  let angle = TWO_PI / npoints;
  let halfAngle = angle / 2.0;
  beginShape();
  for (let a = 0; a < TWO_PI; a += angle) {
    let sx = x + cos(a) * radius2;
    let sy = y + sin(a) * radius2;
    vertex(sx, sy);
    sx = x + cos(a + halfAngle) * radius1;
    sy = y + sin(a + halfAngle) * radius1;
    vertex(sx, sy);
  }
  endShape(CLOSE);
}


